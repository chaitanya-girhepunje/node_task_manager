require("../src/db/mongoose");
const User = require("../src/models/user");

// User.findByIdAndUpdate("5f4643647925b746b4aa98ab", { age: 2 })
//   .then((user) => {
//     console.log(user);
//     return User.countDocuments({ age: 2 });
//   })
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((e) => {
//     console.log(e);
//   });

const updateAgeAndCount = async (id, age) => {
  const user = await User.findByIdAndUpdate(id, { age });
  const count = await User.countDocuments({ age });
  return count;
};

updateAgeAndCount("5f46431181acea52c82b08d7", 3)
  .then((count) => {
    console.log(count);
  })
  .catch((e) => {
    console.log(e);
  });
