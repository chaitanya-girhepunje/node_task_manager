require("../src/db/mongoose");
const Task = require("../src/models/task");

// Task.findByIdAndDelete("5f4738b12de82007402435b6")
//   .then((task) => {
//     console.log(task);
//     return Task.countDocuments({ completed: false });
//   })
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((e) => {
//     console.log(e);
//   });

const deleteTaskAndCount = async (id) => {
  const task = await Task.findByIdAndDelete(id);
  const count = await Task.countDocuments();
  return count;
};

deleteTaskAndCount("5f462ef741e8e40d7489e649")
  .then((count) => {
    console.log(count);
  })
  .catch((e) => {
    console.log(e);
  });
